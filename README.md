# Holder for Zeiss Lightsheet sample chamber

## Project structure

```bash
This repository
    ├── assets # create new folders if needed
    │   ├── drawings # technical drawings
    │   ├── images # pictures or screenshots of the parts
    │   └── models # 2d/3d rendering of parts
    └── src # actual .stl source files
```

## One sentence pitch
![](assets/images/Chamber_holder_metal__Lightsheet_.jpeg){width=40%} ![](assets/models/Chamber_holder_3D_printed__Lightsheet_.jpeg){width=40%}

Used to hold the sample chamber of a Zeiss Lightsheet microscope in an optimal and stable position to mount cover glasses.

## How it works
This object allows to keep the chamber of a Zeiss Lightsheet in a stable horizontal position. This way, the mounting of the cover glasses for the illumination and detection windows is easier. By rotating the chamber along its axis by 90° and re-inserting it into the chamber holder, one window after the other can be installed without further difficulty. The first version of the object was made out of metal produced by milling, the second version was 3D printed with the same dimensions, to lower costs and reduce the weight.

## Authors
Mohammad Goudarzi, Imaging and Optics Facility (IOF), IST Austria, iof@ista.ac.at. 
Astrit Arslani, Machine Shop (MIBA), IST Austria, mshop@ist.ac.at.

## Acknowledgment
The MIBA Machine Shop of the IST Austria for production of the first version of the object, and allowing us to make it available for the public.
Robert Hauschild for 3D-printing the second version.
